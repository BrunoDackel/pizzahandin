public class Olives extends Topping {
    public Olives() {
        super("Olives", 25);
    }

    public Olives(Topping t) {
        super(t);
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Olives(this);
    }
}
