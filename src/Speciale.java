public class Speciale extends Pizza {
    private static final double BASE_PRICE = 90;
    private static final String INGREDIENTS = "Tomato, anchovies, crab";

    public Speciale(PizzaSizes size) {
        super("Speciale", size);
    }

    public Speciale(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Speciale(this);
    }
}
