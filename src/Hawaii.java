public class Hawaii extends Pizza{
    private static final double BASE_PRICE = 75;
    private static final String INGREDIENTS = "Tomato, Cheese, Ham, Pineapple";

    public Hawaii(PizzaSizes size) {
        super("Hawaii", size);
    }

    public Hawaii(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Pizza makeDeepCopy() {
        return new Hawaii(this);
    }
}
