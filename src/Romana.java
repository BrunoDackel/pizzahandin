public class Romana extends Pizza {
    private static final double BASE_PRICE = 95;
    private static final String INGREDIENTS = "Pepperoni, Gorgonzola, Champion";
    public Romana(PizzaSizes size) {
        super("Romana", size);
    }

    public Romana(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Romana(this);
    }
}
