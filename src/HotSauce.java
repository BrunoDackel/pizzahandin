public class HotSauce extends Topping {
    public HotSauce() {
        super("Hot Sauce", 20);
    }

    public HotSauce(Topping t) {
        super(t);
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new HotSauce(this);
    }
}
