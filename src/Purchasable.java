public interface Purchasable {
    String getName();
    double getPrice();
    Purchasable makeDeepCopy();
}
