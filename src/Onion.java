public class Onion extends Topping {

    public Onion() {
        super("Onion", 10);
    }

    public Onion(Topping t) {
        super(t);
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Onion(this);
    }
}
