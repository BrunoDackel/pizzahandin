public class Curry extends Pizza {
    private static final double BASE_PRICE = 60;
    private static final String INGREDIENTS = "Chicken, Corn, Curry";
    public Curry( PizzaSizes size) {
        super("Curry", size);
    }

    public Curry(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Curry(this);
    }
}
