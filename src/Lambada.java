public class Lambada extends Pizza {
    private static final double BASE_PRICE = 55;
    private static final String INGREDIENTS = "Kebab, Pepperoni, Onion";
    public Lambada(PizzaSizes size) {
        super("Labada", size);
    }

    public Lambada(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Lambada(this);
    }
}
