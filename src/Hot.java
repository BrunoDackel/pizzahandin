public class Hot extends Pizza {
    private static final double BASE_PRICE = 65;
    private static final String INGREDIENTS = "Pepperoni, Paprika, Tomato";
    public Hot(PizzaSizes size) {
        super("Hot", size);
    }

    public Hot(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Hot(this);
    }
}
