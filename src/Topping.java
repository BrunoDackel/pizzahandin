public abstract class Topping implements Purchasable {
    private String name;
    private double price;

    public Topping(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Topping (Topping t){
        this.name = t.getName();
        this.price = t.getPrice();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
