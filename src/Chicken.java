public class Chicken extends Pizza {
    private static final double BASE_PRICE = 90;
    private static final String INGREDIENTS = "Chicken, Salat, Creme-fraiche";
    public Chicken(PizzaSizes size) {
        super("Chicken", size);
    }

    public Chicken(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Chicken(this);
    }
}
