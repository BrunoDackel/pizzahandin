import java.util.ArrayList;
import java.util.Scanner;

/**
 * Output was cheked by Michal
 */
public class Main {

    private static ArrayList<Pizzas> listOfAllPizzas = new ArrayList<>();
    private static ArrayList<Topping> listOfAllToppings = new ArrayList<>();
    private static Pizzas orderedPizza;
    private static boolean isComplete = false;
    private static int menuState = 1;

    /**
     * The Main class will start by making a list of all pizzas and toppings on the menu
     * The while loop will be broken when the customer ordered a pizza //TODO Maybe allow the customer to not order?
     * The switch case will keep track of in which step of the oredering process we are
     * @param args
     */
    public static void main(String[] args) {

        initPizzaList();
        initToppingList();
        while (!isComplete) {
            switch (menuState) {
                case 1:
                    takePizzaOrder();
                    break;
                case 2:
                    choosePizzaSize();
                    break;
                case 3:
                    chooseTopping();
                    break;
                case 4:
                    confirmOrder();
                    break;
                case 5:
                    removeTopping();
                    break;
            }
        }
        System.out.println("Thank you for your order!");
    }

    private static void initPizzaList() {
        listOfAllPizzas.add(new Margheritha(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Hawaii(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Italiano(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Speciale(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Lambada(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Flora(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Curry(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Chicken(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Hot(PizzaSizes.NORMAL));
        listOfAllPizzas.add(new Romana(PizzaSizes.NORMAL));
    }

    private static void initToppingList() {
        listOfAllToppings.add(new Onion());
        listOfAllToppings.add(new Cheese());
        listOfAllToppings.add(new HotSauce());
        listOfAllToppings.add(new Olives());
    }

    /**
     * This method will make a formatted list from our listOfAllPizzas Arraylist
     * This method will be called from choosePizzaOrder()
     */
    private static void printPizzaMenu() {
        System.out.println("Pizza Menu:");
        int tempCounter = 1;
        String format = "%02d) %-20s %-50s DKK %.2f%n";
        for (Pizzas pizza : listOfAllPizzas) {
            System.out.printf(format, tempCounter, pizza.getName(), pizza.getListOfingredients(), pizza.getPrice());
            tempCounter++;
        }
    }

    /**
     * The main method a user will interact with the system on
     * We will make sure that the user enters an integer and that this integer is in the range we want
     * @param minInt the minimum integer a user may enter (inclusive)
     * @param maxInt the maximum integer a user may enter (inclusive)
     * @return the integer the user typed in
     */
    private static int getValidUserinput(int minInt, int maxInt) {
        Scanner in = new Scanner(System.in);
        int tempInt;
        do {
            System.out.println("Input a number from the menu: ");
            while (!in.hasNextInt()) {
                System.out.println("This was not a number: ");
                in.next();
            }
            tempInt = in.nextInt();
        } while (!(tempInt >= minInt && tempInt <= maxInt));
        return tempInt;
    }

    /**
     * This method will lead us to choosePizzaSize()
     */
    private static void takePizzaOrder() {
        printPizzaMenu();
        System.out.println("Choose your Pizza: ");
        int userInput = getValidUserinput(1, listOfAllPizzas.size());
        orderedPizza = (Pizzas) listOfAllPizzas.get(userInput - 1).makeDeepCopy();
        menuState = 2;
    }

    /**
     * This method will lead us to chooseToppings() or to the previous menu takePizzaOrder()
     */
    private static void choosePizzaSize() {
        System.out.printf("You ordered a %s Pizza!%n", orderedPizza.getName());
        System.out.println("What size do you want?");
        String format = "%02d) %-20s DKK %.2f%n";
        System.out.printf(format, 1, "Child", orderedPizza.getBasePrice() * 0.75);
        System.out.printf(format, 2, "Normal", orderedPizza.getBasePrice());
        System.out.printf(format, 3, "Family", orderedPizza.getBasePrice() * 1.5);
        System.out.println("04) Go to previous menu.");
        int userInput = getValidUserinput(1, 4);
        switch (userInput) {
            case 1:
                orderedPizza.setSize(PizzaSizes.CHILD);
                menuState = 3;
                break;
            case 2:
                orderedPizza.setSize(PizzaSizes.NORMAL);
                menuState =3 ;
                break;
            case 3:
                orderedPizza.setSize(PizzaSizes.FAMILY);
                menuState=3;
                break;
            case 4:
                menuState = 1;
                break;

        }

    }

    private static void printToppingMenu() {
        int tempCounter = 1;
        System.out.printf("You ordered a %s sized %s Pizza ", orderedPizza.getSize().toString(), orderedPizza.getName());
        if (orderedPizza.getToppings().size() > 0) { //We only print this bit if there is a topping
            System.out.printf("with %s as extra topping %n", orderedPizza.getListOfToppings());
        }
        System.out.println("do you want extra topping? Toppings are affect by pizza size: ");
        String format = "%02d) %-20s DKK %.2f%n";
        for (Topping topping : listOfAllToppings) {
            System.out.printf(format, tempCounter, topping.getName(), topping.getPrice());
            tempCounter++;
        }
        System.out.printf("%02d) Go the previous Menu. %n", tempCounter);
        System.out.printf("%02d) Remove a topping. %n", tempCounter+1);
        System.out.printf("%02d) No more Toppings and go to payment! %n", tempCounter+2);
    }

    /**
     * This method will lead us to confirmOrder()
     * to the previous menu choosePizzaSize()
     * or possibly to remove a topping with removeTopping()
     */
    private static void chooseTopping() {
        printToppingMenu();
        System.out.println("Choose your Topping: ");
        int userInput = getValidUserinput(1, listOfAllToppings.size()+3);
        if (userInput == listOfAllToppings.size()+1){
            menuState = 2;
        }
        else if(userInput == listOfAllToppings.size()+2){
            menuState = 5;
        }
        else if(userInput == listOfAllToppings.size()+3) {
            menuState = 4;
        }
        else {
            orderedPizza.addTopping((Topping) listOfAllToppings.get(userInput - 1).makeDeepCopy());
        }
    }

    /**
     * This method will either end the program or lead us to the previous menu chooseToppings()
     */
    private static void confirmOrder(){
        System.out.printf("you have ordered a %s sized %s pizza ",orderedPizza.getSize().toString(),orderedPizza.getName());
        if (orderedPizza.getToppings().size() > 0) {
            System.out.printf("with %s as extra topping ", orderedPizza.getListOfToppings());
        }
        System.out.printf("for %.2f DKK %n",orderedPizza.getPrice());
        System.out.printf("01) Order Pizza! %n");
        System.out.printf("02) Go to previous Menu %n");
        int userInput = getValidUserinput(1, 2);
        switch (userInput){
            case 1:
                isComplete = true;
                break;
            case 2:
                menuState = 3;
                break;
        }
    }

    /**
     * side menu of chooseToppings()
     * If there is no topping that can be removed, the user will be alreted of his mistake.
     */
    private static void removeTopping(){
        if(orderedPizza.getToppings().size() == 0){
            System.out.println("There are no toppings.");
            menuState = 3;
            return;
        }
        System.out.println("What topping do you want to remove?");
        int tempCounter = 1;
        for (Topping t: orderedPizza.getToppings() ) {
            System.out.printf("%02d) %s %n",tempCounter, t.getName());
            tempCounter++;
        }
        int userInput = getValidUserinput(1,orderedPizza.getToppings().size());
        orderedPizza.removeTopping(orderedPizza.getToppings().get(userInput-1));
        menuState = 3;
    }
}
