public class Italiano extends Pizza{
    private static final double BASE_PRICE = 70;
    private static final String INGREDIENTS = "Tomato, Cheese, pepperoni";

    public Italiano(PizzaSizes size) {
        super("Italiano", size);
    }

    public Italiano(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Italiano(this);
    }
}
