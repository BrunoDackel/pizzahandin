import java.util.ArrayList;

public abstract class Pizza implements Pizzas {
    private String name;
    private double price;
    private PizzaSizes size;
    private ArrayList<Topping> toppings;

    public Pizza(String name, PizzaSizes size) {
        this.name = name;
        this.size = size;
        this.toppings = new ArrayList<>();
        this.price = calculatePrice();
    }

    /**
     * This constructor will make a Deep Copy of the given object in the praramter
     * @param p a pizza
     */
    public Pizza(Pizza p){
        this.name = p.getName();
        this.size = p.getSize();
        this.toppings = p.getToppings();
        this.price = p.calculatePrice();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public PizzaSizes getSize() {
        return size;
    }

    @Override
    public void setSize(PizzaSizes size) {
        this.size = size;
        setPrice(calculatePrice());
    }

    /**
     * This method will be called whenever we change the size or toppings
     * @return the new price
     */
    @Override
    public double calculatePrice() {
        double tempDouble=0;
        switch (size){
            case CHILD:
                tempDouble = getBasePrice() * CHILD_MOD;
                break;
            case NORMAL:
                tempDouble = getBasePrice() * NORMAL_MOD;
                break;
            case FAMILY:
                tempDouble = getBasePrice() * FAMILY_MOD;
                break;
        }

        if (toppings != null) {
            for (Topping t : toppings) {
                tempDouble += t.getPrice();
            }
        }
        return tempDouble;
    }

    @Override
    public void addTopping(Topping topping) {
        toppings.add(topping);
        setPrice(calculatePrice());
    }

    @Override
    public void removeTopping(Topping topping) {
        toppings.remove(topping);
        setPrice(calculatePrice());
    }

    @Override
    public ArrayList<Topping> getToppings() {
        return toppings;
    }

    @Override
    public String getListOfToppings() {
        ArrayList<Topping> tempArraylist = toppings;
        StringBuilder builder = new StringBuilder();
        for (Topping t: tempArraylist) {
            builder.append(t.getName());
            builder.append(" ");
        }
        builder.delete(builder.length()-1,builder.length());
        return builder.toString();
    }
}
