import java.util.ArrayList;

public interface Pizzas extends Purchasable {
    double CHILD_MOD = 0.75;
    double NORMAL_MOD = 1;
    double FAMILY_MOD = 1.5;

    double getBasePrice();
    void  setPrice(double price);
    PizzaSizes getSize();
    void setSize(PizzaSizes size);
    double calculatePrice();
    void addTopping(Topping topping);
    void removeTopping(Topping topping);
    ArrayList<Topping> getToppings();
    String getListOfingredients();
    String getListOfToppings();

}
