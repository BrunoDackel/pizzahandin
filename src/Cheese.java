public class Cheese extends Topping {
    public Cheese() {
        super("Cheese", 15);
    }

    public Cheese(Topping t) {
        super(t);
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Cheese(this);
    }
}
