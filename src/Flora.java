public class Flora extends Pizza {
    private static final double BASE_PRICE = 85;
    private static final String INGREDIENTS = "Turkey, Pepperoni, Champions";
    public Flora(PizzaSizes size) {
        super("Flora", size);
    }

    public Flora(Pizza p) {
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Purchasable makeDeepCopy() {
        return new Flora(this);
    }
}
