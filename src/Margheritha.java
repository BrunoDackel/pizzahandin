public class Margheritha extends Pizza {
    private static final double BASE_PRICE = 60;
    private static final String INGREDIENTS = "Tomato, Cheese";

    public Margheritha(PizzaSizes size) {
        super("Margheritha", size);
    }

    public Margheritha(Pizza p){
        super(p);
    }

    @Override
    public double getBasePrice() {
        return BASE_PRICE;
    }

    @Override
    public String getListOfingredients() {
        return INGREDIENTS;
    }

    @Override
    public Pizza makeDeepCopy() {
        return new Margheritha(this);
    }

}
